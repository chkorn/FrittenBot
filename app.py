#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived from examplebot
Parses Aramark website and responds to some commands
"""

import os
import json
import logging
import holidays

from aramark import get_menu_day
from datetime import datetime, timedelta, time
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Bavarian holidays
BAVARIAN_HOLIDAYS = holidays.CountryHoliday('DE', prov='BY')

MIN_DELAY = timedelta(minutes=1)
CATEGORIES = ['Suppe', 'Mediterran', 'Wok & Grill', 'Aus dem Ofen', 'Traditionell & Klassisch',
              'Hauptgericht 1', 'Hauptgericht 2', 'Hauptgericht 3', 'Hauptgericht 4']
MODIFIERS = {'Aus dem Ofen': u'\U0001F355', 'Suppe': u'\U0001f372'}
MODIFIERS2 = {u'Pasta': u'\U0001f35d', u'Hähnchen': u'\U0001f357', u'Schwein': u"\U0001F437",
              u'Bacon': u'\U0001f953', u'Rind': u'\U0001f402', u'Pommes': u'\U0001f35f', u'kartoffel': u"\U0001F954",
              u'Burger': u'\U0001f354', u'nudeln': u'\U0001f35d', u'reis': u'\U0001F35a'
              }

CACHE = {}
LAST_POST = {}
SUBS = {}


def get_menu(cantina_id):
    if cantina_id in CACHE and CACHE[cantina_id].get('date') == datetime.now().strftime("%F"):
        return CACHE[cantina_id]['menu']
    logger.info("Cache miss: %s", cantina_id)
    raw_menu = get_menu_day(cantina_id=cantina_id)
    if not raw_menu:
        return None
    menu = []
    for cat_meals in raw_menu:
        if cat_meals.get('category') in CATEGORIES:
            for meals in cat_meals.get('meals'):
                mod2 = [match for match in MODIFIERS2 if match.lower() in meals.get('title').lower()]
                if cat_meals.get('category') in MODIFIERS:
                    name = u"{} {}".format(MODIFIERS[cat_meals.get('category')], meals.get('title'))
                elif mod2:
                    name = u"{} {}".format(u"".join([MODIFIERS2[k] for k in mod2]), meals.get('title'))
                else:
                    name = u'\U0001f37d ' + meals.get('title')
                menu.append((name, meals.get('price').get('SWM' if cantina_id == 'aramark_menu2-4' else u'Gäste')))
    CACHE[cantina_id] = {'date': datetime.now().strftime("%F"), 'menu': menu}
    return menu


def get_menu_as_message(header, cantina_id):
    menu_str = u"<b>" + header + "</b>\n"
    menu = get_menu(cantina_id)
    if not menu:
        return("nichts geladen")
    meals = [u": ".join(x) for x in menu]
    menu_str += u"\n".join(meals)
    return menu_str.replace(u"\u00a0EUR", u"\u00a0€")


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def itm(bot, update, args=None):
    """Fetch it@M menu"""
    if not (args and args[0] == "-f") and 'itm' in LAST_POST and LAST_POST['itm'] + MIN_DELAY > datetime.utcnow():
        logger.info("Spamprotection: %s", 'itm')
        return
    update.message.reply_text(get_menu_as_message('IT Rathaus', 'aramark_menu2-4'), parse_mode="HTML")
    LAST_POST['itm'] = datetime.utcnow()


def swm(bot, update, args=None):
    """Fetch SWM menu"""
    if not (args and args[0] == "-f") and 'swm' in LAST_POST and LAST_POST['swm'] + MIN_DELAY > datetime.utcnow():
        logger.info("Spamprotection: %s", 'swm')
        return
    update.message.reply_text(get_menu_as_message('Stadtwerke München', 'aramark_menu2-16'), parse_mode="HTML")
    LAST_POST['swm'] = datetime.utcnow()


def post_subscription(bot, job):
    logger.info("Posting subscription: %r", job.context)

    if datetime.now() in BAVARIAN_HOLIDAYS:
        logger.debug("Stopping post of menu because of holiday!")
        return 

    if job.context.get('cantina') == 'itm':
        text_menu = get_menu_as_message('IT Rathaus', 'aramark_menu2-4')
    elif job.context.get('cantina') == 'swm':
        text_menu = get_menu_as_message('Stadtwerke München', 'aramark_menu2-16')

    bot.send_message(chat_id=job.context.get('chat_id'),
                     text="<i>Es ist %s Uhr, Zeit ans Essen zu denken...</i>\n%s" % (
                         job.context.get('start_time'), text_menu),
                     parse_mode="HTML")


def unschedule_subscription(job_name, job_queue):
    for job in job_queue.get_jobs_by_name(job_name):
        job.schedule_removal()
    if job_name in SUBS:
        logger.info("Unscheduling subscription: %r", SUBS[job_name])
        del(SUBS[job_name])
        return True
    return False


def schedule_subscription(job_name, job_queue, start_time, context):
    logger.info("Scheduling subscription: %r", context)
    job_queue.run_daily(post_subscription,
                        time=start_time,
                        days=(0, 1, 2, 3, 4),
                        context=context,
                        name=job_name)
    SUBS[job_name] = context
    return True


def callback_sub(bot, update, job_queue, args=None):
    """ Generate a regular posting
        TODO: serialize and persist
    """

    if not args or args[0] not in ['itm', 'swm']:
        update.message.reply_text("Syntax: /abo itm|swm [hh:mm]", parse_mode="HTML")
        return

    job_name = str(update.message.chat_id) + "_" + args[0]
    if len(args) == 1:
        # Remove existing job
        if unschedule_subscription(job_name, job_queue):
            update.message.reply_text("Totale Automatisierung deaktiviert. 😭", parse_mode="HTML")
    else:
        try:
            start_time = time.fromisoformat(args[1])
        except ValueError:
            update.message.reply_text("Zeit ungültig.", parse_mode="HTML")
            return
        save_context = {'chat_id': update.message.chat_id, 'cantina': args[0], 'start_time': start_time.strftime("%H:%M")}
        if job_name in SUBS:
            # Remove and reschedule for changes
            unschedule_subscription(job_name, job_queue)
        if schedule_subscription(job_name, job_queue, start_time, save_context):
            update.message.reply_text("Totale Automatisierung aktiviert. 🤩🤩🤩", parse_mode="HTML")
    save_subs()


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def log(bot, update):
    logger.info(u"%r", update.message.text)


def load_subs():
    global SUBS
    filename = os.environ.get("STORAGE", "app.json")
    try:
        with open(filename, "r+") as storage:
            SUBS = json.load(storage)
    except Exception as e:
        logger.exception(e)
        logger.info("Failed restoring subscriptions")
        SUBS = {}
    else:
        logger.info("Restored %s subscriptions", len(SUBS))


def save_subs():
    filename = os.environ.get("STORAGE", "app.json")
    try:
        with open(filename, "w") as storage:
            json.dump(SUBS, storage)
    except Exception as e:
        logger.exception(e)
        logger.info("Failed persisting subscriptions")
    else:
        logger.info("Persisted %s subscriptions", len(SUBS))


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(os.environ.get("TOKEN"))

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("itm", itm, pass_args=True))
    dp.add_handler(CommandHandler("swm", swm, pass_args=True))
    dp.add_handler(CommandHandler('abo', callback_sub, pass_args=True, pass_job_queue=True))
    # dp.add_handler(MessageHandler(Filters.text, log))
    # dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    load_subs()
    for job_name, context in SUBS.items():
        schedule_subscription(job_name,
                              updater.job_queue,
                              time.fromisoformat(context.get('start_time')),
                              context)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
