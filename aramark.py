from bs4 import BeautifulSoup
import requests
from datetime import datetime


def load_menu_week(cantina_id):
    html = requests.post("http://mein.aramark.de/stadtwerke-muenchen/wp-admin/admin-ajax.php", data={
        'action': 'aramark_menu2_content',
        'id': cantina_id
    }).text
    soup = BeautifulSoup(html, "html5lib")
    menu = {}
    all_days = soup.select("div[data-aramark_menu2-day]")
    for day in all_days:
        date = day.attrs['data-aramark_menu2-day']
        menu[date] = []
        categories = day.select("div.aramark_menu2-counter")
        for category in categories:
            category_name = category.select_one(
                "div.aramark_menu2-countertitleleft").text
            meals = []
            for gericht in category.select("div.aramark_menu2-countermenu"):
                node_title = gericht.select_one(
                    "div.aramark_menu2-menutitle > h4")
                node_text = gericht.select_one(
                    "div.aramark_menu2-menutext > p")
                title = node_title.text if node_title else ""
                title += " " if node_title and node_text else ""
                title += node_text.text if node_text else ""
                prices = gericht.select("p.aramark_menu2-menuprice")
                flags = [[flag.attrs.get('alt'), flag.attrs.get('src').replace("http", "https")]
                         for flag in gericht.select("div.aramark_menu2-menuaddons > img[alt]")]
                try:
                    meals.append({
                        'title': title,
                        'price': dict([prices[0].get_text(separator="\t", strip=True).split("\t")[:2], prices[1].get_text(separator="\t", strip=True).split("\t")[:2]]),
                        'flags': flags
                    })
                except IndexError:
                    pass
            menu[date].append({
                'category': category_name,
                'meals': meals
            })
    return menu


def get_menu_day(cantina_date=None, cantina_id="aramark_menu2-4"):
    if not cantina_date:
        cantina_date = datetime.now().date().isoformat()
    menu = load_menu_week(cantina_id)
    if cantina_date not in menu:
        return None
    else:
        return menu.get(cantina_date)
