FROM python:3.7-alpine

WORKDIR /usr/src/app
ADD requirements.txt /usr/src/app/requirements.txt
COPY . /usr/src/app

RUN apk add -U tzdata\
  && apk --update add --virtual build-dependencies libffi-dev openssl-dev python-dev py-pip build-base \
  && pip install --upgrade pip \
  && pip install --no-cache-dir -r requirements.txt \
  && apk del build-dependencies
  
CMD ["python", "app.py"]
